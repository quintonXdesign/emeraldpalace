//hold music
var holdAudioElement = document.createElement('audio');
holdAudioElement.setAttribute('src', 'audio/048003901-let-game-show-begin.mp3')
holdAudioElement.addEventListener("load", function() {
	
}, true);

$('#music-btn').on('click', function(){
	console.log('clicked');
	if (holdAudioElement.paused == false) {
	      holdAudioElement.pause();
	      // alert('music paused');
	  } else {
	  	  holdAudioElement.loop = true;
	      holdAudioElement.play();
	      // alert('music playing');
	  }
	
});

// ==============================================
/* SVG Floorplan Toggle */
// ==============================================

$('#Machines path, #Machines rect, #Machines polygon').on('click', function(){

	this.classList.toggle('on');
	
});

$('#select-all-btn').on('click', function(){

	$('#Machines path, #Machines rect, #Machines polygon').each(function(i){
		this.classList.add('on');
	});

});


$('#signin-form').submit(function(e){

	form = this;
	formWrapper = $('.fun-wrapper .input-group');
	$('#signin-button').addClass('on');
	e.preventDefault();
	formUp(formWrapper);
	setTimeout(function(){	
		
		form.submit();
		
	}, 1500);
	
});

var gameAssetsReady = false;
$('#lets-play').on('click',function(e){

	var playLink = this.getAttribute('href');
	



	e.preventDefault();
	var summary = $('#summary-overlay');
	formUp(summary);
	setTimeout(function(){
		// window.location = playLink;
		$('.floorplan .fun-wrapper').removeClass('game-off').addClass('game-on');
		
		randomWinner();
		bbWinner();
		holdAudioElement.pause();

		var audioElement = document.createElement('audio');
		var song = ''

        audioElement.setAttribute('src', '');
        audioElement.setAttribute('autoplay', 'autoplay');
        audioElement.volume = 0.1;

        $.get();

        audioElement.addEventListener("load", function() {
            audioElement.play();
        }, true);

		if(gameselected == "Bingo Balls"){

			audioElement.setAttribute('src', 'audio/030078775-50s-game-show.mp3')
			audioElement.addEventListener("load", function() {
	            audioElement.play();
	        }, true);
	        // audioElement.volume = 0.1;

			$('#title-balls, #winner, .bingo-title, .ray-bg, #bb-grid').removeClass('hide');
			$('.fun-wrapper').addClass('hide');
			
			testTL();

		}

		if(gameselected == "Balloon Pop"){

			audioElement.setAttribute('src', 'audio/001384977-game-show-full-version.mp3')
			audioElement.addEventListener("load", function() {
	            audioElement.play();
	        }, true);

			$('.bp-bg, #bp-title').removeClass('hide');
			
			shuffle(tileRepo);

			bpTL();


		}
		if(gameselected == "Space Wars"){

			audioElement.setAttribute('src', 'audio/000733879-preparing-battle.mp3')
			audioElement.addEventListener("load", function() {
	            audioElement.play();
	        }, true);
	        audioElement.volume = 0.5;

			$('.sw-bg, #sw-title').removeClass('hide');
			$('#bb-grid').addClass('sw-on');
			// shuffle(tileRepo);

			swTL();

		}
		
	}, 1600);
});

var formUp = function(ele){
	TweenLite.to(ele, .4, { ease: Back.easeIn.config(1.7), y: -1000, delay: 1 });
};

var clearPlayers = function(){

		var playersArr = totalPlayers()[0];

		for(i = 0; i < playersArr.length; i++){
			playersArr[i].classList.toggle('on');
		}
		
};

// ==============================================
/* GAME PLAYERS */
// ==============================================

//GLOBAL GAME VARS
var container = $('.grid-wrapper')[0];
var duration = 0.5;  
var delay = 0.03; 



var tiles = [];
var ease  = Back.easeOut.config(0.5);

//bppop
var winTop = '15%';
var winLeft = '50%';
var winWidth = '40%';
//END GLOBAL GAME VARS

var totalPlayers = function(){
	var players = document.querySelectorAll('#main-floorplan .on');
	var playersArr = shuffle([].slice.call(players));

	var gameselected = ["Bingo Balls", "Balloon Pop"];
	// var gameselected = "Balloon Pop";

	if( playersArr.length <= 24 ){
			gameselected = ["Bingo Balls", "Balloon Pop", "Space Wars"];
			gameselected = gameselected[Math.floor(Math.random()*gameselected.length)];
			
		} 
		if( playersArr.length > 24 ){
			gameselected = gameselected[Math.floor(Math.random()*gameselected.length)];
		}

	// gameselected = "Balloon Pop"	

	function gameSelect(){
		// if( gamePlayers.length >= 9 ){
		// 	gameselected[Math.floor(Math.random()*gameselected.length)];
			
		// } else {
		// 	gameselected = "Space Wars";
		// }	
		// if( gamePlayers.length <= 8 ){
		// 	gameselected = "Space Wars";
			
		// } 
		// if( gamePlayers.length > 8 ){
		// 	gameselected = gameselected[Math.floor(Math.random()*gameselected.length)];
		// }
	}
	
	$('#total-players').text(players.length);
	$('#game-title').text(gameselected);


	if( playersArr.length <= 20 ){
		// bballsRow = 4;
		dropNum = 3;
		popSpeed = .5;
		// winTop = '-50%';
		// winLeft = '35%';
		// winWidth = '40%';
		winTop = '-15%';
			winLeft = '35%';
			winWidth = '35%';
		$('.grid-wrapper').addClass('row-4');
		if(playersArr.length >= 1 && playersArr.length <= 4){
			$('.grid-wrapper').addClass('game-rows-1');
		}
		if(playersArr.length >= 5 && playersArr.length <= 8){
			$('.grid-wrapper').addClass('game-rows-2');
		}
		if(playersArr.length >= 9 && playersArr.length <= 12){
			$('.grid-wrapper').addClass('game-rows-3');
		}
		if(playersArr.length >= 13 && playersArr.length <= 16){
			$('.grid-wrapper').addClass('game-rows-4');
		}
		if(playersArr.length >= 17 && playersArr.length <= 20){
			$('.grid-wrapper').addClass('game-rows-5');
			
		}

	} else {
		// playersArr.length = 8;
		dropNum = 6;
		$('.grid-wrapper').addClass('row-8');
		if(playersArr.length >= 21 && playersArr.length <= 24){
			$('.grid-wrapper').addClass('game-rows-3');
		}
		if(playersArr.length >= 25 && playersArr.length <= 32){
			$('.grid-wrapper').addClass('game-rows-4');
		}
		if(playersArr.length >= 31 && playersArr.length <= 40){
			$('.grid-wrapper').addClass('game-rows-5');
		}
	}

	return [playersArr, gameselected];
};

//pick random game



$('#clear-btn').click(function(){
	clearPlayers();
});

var randomWinner = function(){

	var newbbArr = totalPlayers()[0];
	winNum = newbbArr[0].id.substr(2);	//return id without namespace

	var tilesArr = [[].slice.call(tiles)];

	return [newbbArr, winNum, tilesArr];
	// return [newbbArr, winner];
};


function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

// ==============================================
/* BINGO BALLS GRID */
// ==============================================
// var tl = new TimelineLite();

var bingoBalls = function(){
	
	console.log('bingoballs');
	$('.overlay-container').trigger('click');

}

var bbWinner = function(){
	var winner = winNum;

	$('#winner :not(.not-ball)').attr('src', 'css/img/bb-' + winner + '.png');

}

// ==============================================
/* SUMMARY TOGGLE */
// ==============================================
openModal = function(){
	$('#next-btn').on('click',function(e){
		e.preventDefault();
		// totalPlayers();
		
		randomWinner();
		init();


		$('.funwrapper').addClass('hide');
		$('#summary-overlay').removeClass('hide');
		$('.overlay-container').fadeToggle();
		$('#summary-overlay').addClass('is-visible');
		setTimeout(function(){
			$('#loading-button').fadeOut('slow');
		}, 5000);
		
	});
};

closeModal = function(){
	$('#back-to-floorplan').click(function(){
		$('#bb-grid .grid-wrapper').empty();
		$('.overlay-container').fadeToggle();
		$('#summary-overlay').removeClass('is-visible');
		// $(form).animate({top: "40%"});
		setTimeout(function(){
			$('#summary-overlay').addClass('hide');
		}, 400);		
	});
};

openModal();
closeModal();

// ==============================================
/* GSAP*/
// ==============================================
// TweenLite.to(".wrapper", 1, {css:{autoAlpha:1}});


// Space Wars Timeline
// ==============================================
var swTL = function(){
	console.log('swtl');
	
	var swTitle = $('#sw-title'),
		swTitle2 = $('#sw-title-2'),
		swStars = $('#stars-bg'),
		asteroids = $('.bp-holder');

	var swMaster = new TimelineLite();

	function sceneOne(){
		var tl = new TimelineLite();

		tl
			.to(swStars, 1.75, { scaleX: 1, scaleY: 1 }, "begin")
			.from(swTitle, 1.75, { scaleX: 0, scaleY: 0, ease: Power0.easeNone, y: 0 }, "titleIn-=1")
			.to([swTitle, swStars], 1, { scaleX: 3, scaleY: 3, autoAlpha: 0, delay: 2, ease: Power4.easeIn, y:0 }, "titleOut-=.7")
			.to(asteroids, 1, { display: 'inline-block' })
			.set($('#rocket'), {className: '-=hide' }, "revealRocket")
			.from($('#rocket'), .5, { y: '100%' }, "revealRocket")
			
		 ;

		return tl;
	}

	var swMasterTL = function(){
		swMaster
			.add(sceneOne(), "sceneOne")
			.add(startSpaceWars, "sceneSWGrid")
		;
	};
	swMasterTL();
};	
// if(gameselected == "Space Wars"){bpTL2()}
var swTL2 = function(){
	// console.log('bptl');
	var swTitle1 = $('#sw-title-1'),
		swTitle2 = $('#sw-title-2'),
		swWinner = $('.sw-winner-title'),
		asteroids = $('.not-winner'),
		rocket = $('#rocket'),
		winningAsteroid = $('#bb-grid .winner');

	var swMaster2 = new TimelineLite();


		swMaster2
		  .add('swWinnerScene')
		  .add(playApplause, "swWinnerScene+=1.75")
		  .set(winningAsteroid, {animation: 'none'}, "swWinnerScene+=1.75")
		  .to(winningAsteroid, 1, {position: 'absolute', width: winWidth, top: '12%', left: '45%', x:'-50%', y:'-50%'}, "swWinnerScene+=1.75")
		  .to($('.sw-bg'), 15, {backgroundSize: '+=250% +=250%', ease: Power0.easeNone, y: 0 }, "swWinnerScene+=1.75")
		  .to(rocket, 1, {y: '-650%', ease: Power4.easeIn}, "swWinnerScene+=2")
		  .to($('#sw-winner-star'), 2, { x: '1500px' , y: '-1000px', autoAlpha: 0}, "swWinnerScene+=2.5")
		  .to($('#sw-winner-star1'), 2, { x: '1500px' , y: '-1000px', autoAlpha: 0}, "swWinnerScene+=3.1")
		  .to($('#sw-winner-star2'), 2, { x: '1500px' , y: '-1000px', autoAlpha: 0}, "swWinnerScene+=10.8")
		  .to($('#sw-winner-star3'), 2, { x: '1500px' , y: '-1000px', autoAlpha: 0}, "swWinnerScene+=4")
		  .to($('#sw-winner-star4'), 2, { x: '-1500px' , y: '1000px', autoAlpha: 0}, "swWinnerScene+=5")
		  .to($('#sw-winner-star5'), 2, { x: '1500px' , y: '-1000px', autoAlpha: 0}, "swWinnerScene+=6.6")
		  .to($('#sw-winner-star6'), 2, { x: '-1500px' , y: '1000px', autoAlpha: 0}, "swWinnerScene+=7.25")
		  .to($('#sw-winner-star7'), 2, { x: '-1500px' , y: '-1000px', autoAlpha: 0}, "swWinnerScene+=8.99")
		  .to($('#sw-winner-star'), 2, { x: '1500px' , y: '-1000px', autoAlpha: 0}, "swWinnerScene+=2.5")
		  .to($('#sw-winner-star'), 2, { x: '1500px' , y: '-1000px', autoAlpha: 0}, "swWinnerScene+=2.5")

		  .to(swWinner, 1, {scaleX: 1, scaleY: 1, transformOrigin: 'bottom'}, "swWinnerScene+=2")
		  .to(swWinner, 1, {rotationY: 720, transformOrigin: 'center', repeat: 18}, 'swWinnerScene+=3.3')
		  .to(winningAsteroid, 10, {scaleX:1.5, scaleY:1.5}, "swWinnerScene+=2.5")
		 ;

		 TweenLite.delayedCall(22, goHome);

};

// Balloon Pop Timeline
// ==============================================
var bpTL = function(){
	
	var bpTitle1 = $('#bp-title-1'),
		bpTitle2 = $('#bp-title-2'),
		balloons = $('.bp-holder');

	var bpMaster = new TimelineLite();

	function sceneOne(){
		var tl = new TimelineLite();

		tl
			.fromTo(bpTitle1, .6,{scaleX: 1, scaleY: 0},{scaleX: 1, scaleY: 1, transformOrigin: 'bottom' }, "titleIn")
			.fromTo(bpTitle2, .7,{scaleX: 0, scaleY: 0},{scaleX: 1.3, scaleY: 1.3, autoAlpha: 1}, "titleIn")
			.to(bpTitle2, .2,{scaleX: 1, scaleY: 1})
			.to(bpTitle1, .5, { y: -1000, ease: Back.easeIn.config(1)}, "titleOut+=2" )
			.to(bpTitle2, .5, { y: -1000, ease: Back.easeIn.config(1)}, "titleOut+=2" )
			.to(balloons, 1, { display: 'inline-block' })
		 ;

		return tl;
	}

	var bpMasterTL = function(){
		bpMaster
			.add(sceneOne(), "sceneOne")
			.add(bpSequence, "sceneBpGrid")
		;
	};
	bpMasterTL();
};
// if(gameselected == ""){}
var bpTL2 = function(){
	// console.log('bptl');
	var bpTitle1 = $('#bp-title-1'),
		bpTitle2 = $('#bp-title-2'),
		bpWinner = $('.bp-winner-title'),
		balloons = $('.not-winner')
		winningBalloon = $('#bb-grid .winner');

	var bpMaster2 = new TimelineLite();


		bpMaster2
	// 	tl
		  .to(balloons, 1, {autoAlpha:0})
		  .to(winningBalloon, 1, {position: 'absolute', width: winWidth, top: winTop, left: winLeft, x:'-50%', y:'-50%'}, "bpWinnerScene")
		  // .fromTo(bpWinner, 1, {autoAlpha:0, scaleX: 0, scaleY:0}, { autoAlpha:1, scaleX: 1.3, scaleY:1.3 }, "bpWinnerScene")
		  // .to(bpWinner, .2,{scaleX: 1, scaleY: 1})
		  .to(bpWinner, .6, {autoAlpha:1, scaleX:1, scaleY:1, transformOrigin: 'center'})
		  // .to(bpWinner, 18, {rotationY: 5760, transformOrigin: 'center'}, 'bpFinale')
		  .set($('#bb-grid .winner .bp'), {className: '+=pulsing' }, 'bpFinale')
		  .set(bpWinner, {className: '+=pulsing' }, 'bpFinale')
		  // .set($('#bb-grid .winner '), {className: '+=zindex' }, 'bpFinale')
		 ;

		 TweenLite.delayedCall(15, goHome);

};

// Bingo Balls Timeline
// ==============================================
var testTL = function(){

	var bingoTitle = $('.bingo-title'),
		titleBB1 = $('.title-bb-1'),
		titleBB2 = $('.title-bb-2'),
		titleBB3 = $('.title-bb-3'),
		titleBB4 = $('.title-bb-4'),
		gameGrid = $('#bb-grid'),
		titleBB5 = $('.title-bb-5'),
		gameBalls = $('.bb-grid-ball-num'),
		winnerBB1 = $('.winner-bb-1'),
		winnerBB2 = $('.winner-bb-2'),
		winnerBB3 = $('.winner-bb-3'),
		winnerBB4 = $('.winner-bb-4'),
		winnerBB5 = $('.winner-bb-5'),
		winnerTitle = $('.winner-title'),
		winnerFriends = $('.winner-friends');
	
	var master = new TimelineLite();

	function sceneOne() {

		var tl = new TimelineLite();

		tl.from(bingoTitle, 1.5, {y:'-1000px',ease: Bounce.easeOut})
		  .fromTo(titleBB1, .6, { x: '1500px'}, { x: 0, ease: Back.easeOut.config(1.1), y: 0 })
		  .fromTo(titleBB2, .6, { x: '1500px'}, { x: -50, ease: Back.easeOut.config(1.1), y: 0 })
		  .fromTo(titleBB3, .6, { x: '1500px'}, { x: -80, ease: Back.easeOut.config(1.1), y: 0 })
		  .fromTo(titleBB4, .6, { x: '1500px'}, { x: -110, ease: Back.easeOut.config(1.1), y: 0 })
		  .fromTo(titleBB5, .6, { x: '1500px'}, { x: -140, ease: Back.easeOut.config(1.1), y: 0 })
		  .to(bingoTitle, .15, {scaleX:1.1, scaleY: 1.1}, 4)
		  .to(bingoTitle, .15,{scaleX:1,scaleY:1})
		 ;

		return tl;
	}
	function sceneTwo(){

		var tl = new TimelineLite();

		tl.call(ballBounce, [titleBB1, 0.1, 0.6, -130, 0, 0, 0], "ballDrop")
		  .call(ballBounce, [titleBB2, 0.5, 0.5, -130, 0, 0, 0], "ballDrop")
		  .call(ballBounce, [titleBB3, 0.3, 0.7, -130, 0, 0, 0], "ballDrop")
		  .call(ballBounce, [titleBB4, 0.1, 0.85, -130, 0, 0, 0], "ballDrop")
		  .call(ballBounce, [titleBB5, 0.5, 0.5, -130, 0, 0, 0], "ballDrop")
		;

		return tl;
	}
		
	// tl.add("dropOff");
	function sceneThree(){
		var tl = new TimelineLite();

		tl.to(titleBB1, .8, { top: 1200, ease: Back.easeIn }, "dropOff-.5")
		  .to(titleBB2, .8, { top: 1200, ease: Back.easeIn }, "dropOff-=.7")
		  .to(titleBB3, .8, { top: 1200, ease: Back.easeIn }, "dropOff-=.5")
		  .to(titleBB4, .8, { top: 1200, ease: Back.easeIn }, "dropOff")
		  .to(bingoTitle, .8, { y: 1200, ease: Back.easeIn }, "dropOff-=.7")
		  .to(titleBB5, .8, { top: 1200, ease: Back.easeIn }, "dropOff-=.5")
		  // .to(gameBalls, 1, { display: 'inline-block' }, "showBalls")
		  // .set(gameGrid, {className: 'start-shuffle'})
		;

		return tl;

	}

	function sceneGrid(){
		var tl = new TimelineLite();

		tl
			.to(gameBalls, 1, { display: 'inline-block' }, "showBalls")
			.set(gameGrid, {className: 'start-shuffle'})
		;

		return tl;
	}

	// ==============================================
	/* BINGO BALLS MASTER TINELINE*/
	// ==============================================

	var masterTL = function(){
		master
			.add(sceneOne(), "sceneOne")
			.add(sceneTwo(), "sceneTwo")
			.add(sceneThree(), "sceneThree")
			.add(sceneGrid(), "sceneGrid")
			.add(startBingoBalls, "sceneBingoBalls")
		;
	};
	masterTL();

	function ballFinal(ele, dur1, dur2, y1, y2, scaleX, scaleY, del, rep){
		var timeline = new TimelineMax({delay: del, repeat: rep})

		timeline.add('titleIn+=2');

		timeline.to(ele, dur1, {
			transformOrigin: "50% 50%",
			y:y1,
			ease: Circ.easeOut
		}, "bounceFinal")

				.to(ele, dur2, {
			transformOrigin: "50% 50%",
			y:y2,
			ease: Circ.easeIn
		}, "bounceFinal2")

				.to(ele, 0.15, {
	       transformOrigin: "top",
	       scaleX: scaleX,
	       scaleY: scaleY,
	       ease: Power1.easeInOut
	       //ease: Bounce.easeOut
	     }, "bounce3-=0.04")
	}

	function ballBounce(ele, dur1, dur2, y1, y2, del, rep) {

	     var timeline = new TimelineMax({delay: del, repeat:rep});

	     timeline

	     /* ball bounce up */
	       .to(ele, dur1, {
	       transformOrigin: "50% 50%",
	       y: y1,
	       ease: Circ.easeOut
	       //ease: Power1.easeInOut
	     }, "bounce")

	     /* ball bounce down */
	     .to(ele, dur2, {
	       transformOrigin: "50% 50%",
	       y: y2,
	       ease: Bounce.easeOut
	         //ease: Power1.easeInOut
	     }, "bounce2")

	     return timeline;
	   }

};
function goHome(){
		window.location = '/';
	}
var testTL2 = function(){

	var winningBall = $('.grid-wrapper .winner')
		winnerBB1 = $('.winner-bb-1'),
		winnerBB2 = $('.winner-bb-2'),
		winnerBB3 = $('.winner-bb-3'),
		winnerBB4 = $('.winner-bb-4'),
		winnerBB5 = $('.winner-bb-5'),
		gameGrid = $('#bb-grid'),
		winnerTitle = $('.winner-title'),
		winnerFriends = $('.winner-friends');
	
	var master2 = new TimelineLite();
	
	function sceneFour(){

		// TweenMax.set($('.winner-friends'), {y: -1000});
		TweenMax.set(winnerTitle, {perspective: 400});

		var tl = new TimelineLite();

		tl.to($('.not-winner'), 1, { y: 1000, autoAlpha: 0, ease: Power2.easeInOut })
		  .to(winningBall, 1, {top: '50%', left: '50%', x: '-50%', y: '-102%', width: '25%', position: 'absolute', scaleX: 1.5, scaleY:1.5})

		  // .to(winningBall, 1, {top: '50%', left: '50%', scaleX: 1.5, scaleY:1.5})
		  .fromTo(winnerTitle, 1,{scaleX: 0, scaleY: 0},{scaleX: 1, scaleY: 1, autoAlpha: 1}, "titleIn")
		
		  .call(ballFinal, [winningBall, 0.5, 0.5, -50, 0, 1.75, 1.4, 0, 12] )
		
		  .to(winnerTitle, 1, {rotationY: 720, transformOrigin: 'center', repeat: 12}, 'titleIn+=2')
		  .to(winnerTitle, 1, { scaleX: 2, scaleY: 2, transformOrigin: 'center', autoAlpha: 0}, 'allOut-= .5')
		  .to([winningBall, winnerTitle], 1, { scaleX: 2, scaleY: 2, transformOrigin: 'center', autoAlpha: 0}, 'allOut-= .5')
		  .set(gameGrid, {className: '+=game-over'})
		  .call(goHome)

		;

		return tl;
	}

	// ==============================================
	/* MASTER TINELINE*/
	// ==============================================

	var masterTL2 = function(){
		master2
			.add(sceneFour(), "sceneFour")
		;
	};
	masterTL2();

	function goHome(){
		window.location = '/';
	}

	function ballFinal(ele, dur1, dur2, y1, y2, scaleX, scaleY, del, rep){
		var timeline = new TimelineMax({delay: del, repeat: rep})

		timeline.add('titleIn+=2');

		timeline.to(ele, dur1, {
			transformOrigin: "50% 50%",
			y:y1,
			ease: Circ.easeOut
		}, "bounceFinal")

				.to(ele, dur2, {
			transformOrigin: "50% 50%",
			y:y2,
			ease: Circ.easeIn
		}, "bounceFinal2")

				.to(ele, 0.15, {
	       transformOrigin: "top",
	       scaleX: scaleX,
	       scaleY: scaleY,
	       ease: Power1.easeInOut
	       //ease: Bounce.easeOut
	     }, "bounce3-=0.04")
	}

};

var boxes = [];
var numBoxes = 18
var numBoxesToAdd = 6;
var newbball = [];
var winNum = 0;

var winningScene = false;
var gameLoop = false;

// Animation settings
var duration = 0.5;   
var delay = 0.03;

var tiles = [];
var ease  = Power2.easeInOut;
var container = $('.grid-wrapper')[0];

// $(document).ready(function () {
  
var startBingoBalls = function(){
  // $('.overlay-container').on('click', function(){
  	if(gameselected == "Bingo Balls") {		
  		
  		if(randomWinner()[0].length > 0){

		//remove winner from tileRepo
  			for(var j = 0; j < tileRepo.length; j++){
  					
  				if(tileRepo[j].id.substr(3) == winner)
  					// item = arrballs[i];	
  					tileRepo.splice(j, 1);

  			}

	  		var i = 1;

	  		function bbLoop () { 
	  		   setTimeout(function () { 
	  		     //  call a 2s setTimeout when the loop is called
	  		     if($('#bb-grid').hasClass('start-shuffle')){

		  		      reorderTiles(true);
		  		      setTimeout(dropTiles,600)
		  		      
		  		 }

	  		      i++;
	  		      if (tileRepo.length > 0) {
	  		      	setTimeout(function(){
	  		      		console.log('in the loop');
	  		      		bbLoop();
	  		      	}, 600);

	  		         gameLoop = true ;
	  		         
	  		      }

	  		      //check if array length is empty once game starts
	  		      setTimeout(function(){
	  		      	if (gameLoop == true && tileRepo.length == 0) {
	  		      		
	  		      		$('#winner').addClass('on');
							
							setTimeout(realTestTL2,1000);
							
	  		      	}
	  		      }, 1000);
	  		      
	  		   }, 2000)
	  		}
	  		bbLoop();
	  		winningScene = true;
	  		
	  	}
  	}
  // });
};

function startSpaceWars(){

	if( gameselected == "Space Wars" ){

		console.log('space wars started');

		// TweenMax($('.sw-holder'), 1, { x: '1000px' });
		// console.log('sw-holder');

//**************************BEGIN
		var floatOn = function(){
			console.clear();
			var log = console.log.bind(console);

			var bounce = -1;

			var vMin = 600;
			var vMax = 800;

			var minX = -400; 
			var minY = -150; 
			// var maxX = window.innerWidth;
			// var maxY = window.innerHeight;
			var maxX = document.querySelector('.wrapper').offsetWidth - 1200;
			var maxY = document.querySelector('.wrapper').offsetHeight - 500;
			console.log(maxX)
			console.log(maxY)

			// var baseSVG   = select("#hidden-svg > svg");
			var baseSVG   = select("#rocket");
			var blastOff  = select("#blast-off");
			var container = select("#bb-grid");

			// Map rocket objects to an array
			var rockets  = selectAll(".bp-holder").map(Rocket);

			// blastOff.addEventListener("click", launchRockets);
			window.addEventListener("resize", resize);

			// Rocket factory function
			function Rocket(element) {
			    
			  // Clone hidden SVG and add to element
			  // element.appendChild(baseSVG.cloneNode(true));
			  
			  var width  = element.offsetWidth;
			  var height = element.offsetHeight;
			  
			  TweenLite.set(element, {
			    x: random(width,  maxX - width),
			    y: random(height, maxY - height),
			    autoAlpha: 1
			  });
			    
			  var tracker = ThrowPropsPlugin.track(element, "x,y");
			  
			  // Rocket object to return 
			  var rocket = {
			    element: element,
			    launch: createTween
			  };
			  
			  function createTween(vx, vy) {
			    
			    if (vx == null) vx = "auto";
			    if (vy == null) vy = "auto";
			    
			    var throwProps = {
			      x: { velocity: vx },
			      y: { velocity: vy },
			      resistance: .05
			    };

			    ThrowPropsPlugin.to(element, {
			      throwProps: throwProps,
			      onUpdate: checkBounds
			    });    
			  }
			  
			  function checkBounds() {
			    
			    var x = element._gsTransform.x;
			    var y = element._gsTransform.y;

			    var vx = null;
			    var vy = null;

			    if (x < minX) {

			      vx = tracker.getVelocity("x") * bounce;
			      x  = minX;

			    } else if (x + width > maxX) {

			      vx = tracker.getVelocity("x") * bounce;
			      x  = maxX - width;
			    }

			    if (y < minY) {

			      vy = tracker.getVelocity("y") * bounce;
			      y  = minY;

			    } else if (y + height > maxY) {

			      vy = tracker.getVelocity("y") * bounce;
			      y  = maxY - height;
			    }

			    // Hit detected!!!
			    if (vx != null || vy != null) {

			      TweenLite.set(element, { x: x, y: y });
			      
			      createTween(vx, vy);
			    }
			  }
			  
			  return rocket;
			}

			function launchRockets() {
			    
			  rockets.forEach(function(rocket) {    
			    var vx = random(vMin, vMax) * randomSign();
			    var vy = random(vMin, vMax) * randomSign();    
			    rocket.launch(vx, vy);
			  });
			}
			// $('.sw-holder #rocket').addClass('hide');
			launchRockets();

			function resize() {  
			  maxX = window.innerWidth; 
			  maxY = window.innerHeight; 
			}

			function select(query) {
			  return document.querySelector(query);
			}

			function selectAll(query) {
			  return Array.prototype.slice.call(document.querySelectorAll(query));
			}

			function random(min, max) {
			  return Math.floor(Math.random() * (max - min + 1)) + min;
			}

			function randomSign() {
			  return Math.random() < 0.5 ? -1 : 1;
			}


		};
		floatOn();

//**************************END


		var i = 1;

		setTimeout(function(){

			function swPopLoop () {
			// setTimeout(function(){

		   setTimeout(function () {
		      
		   		var popped = []
		   		// for(var i = 0; i < tileRepo.length; i++){
		   			var randTile = tileRepo[0];
		   			var randTilePos = randTile.getBoundingClientRect();
		   			var rocket = document.getElementById('rocket');
		   			var rocketPos = rocket.getBoundingClientRect();
		   			var offsetX = randTilePos.left;
		   			var offsetY = randTilePos.top;
		   			var rocketOffsetX = rocketPos.left;
		   			var rocketOffsetY = rocketPos.top;
		   			var diffOffsetY = (rocketOffsetY - offsetY) * -1;

		   			if(tileRepo.length != 0){

		   				//don't remove the winning balloon
		   				// if(randTile.childNodes[0].classList.contains('winner')){
		   				// 	randTile = tileRepo[1];
		   				// 	console.log('winner');
		   				// 	console.log(tileRepo[0].childNodes[0]);
		   				// }


	   					if(randTile.childNodes[1].classList.contains('sw-pop')){
	   						console.log(randTile);
	   						console.log('rocket pos');
	   						console.log(rocketPos);
	   						console.log('asteroid pos');
	   						console.log(randTilePos);
	   						var tl = new TimelineLite();
	   							tl
	   								.to($('#rocket'), .45, { x: offsetX, ease: Power2.easeIn })
	   								.set($('#laser-beam'), { opacity: 1 })
	   								.to($('#laser-beam'), .35, { y: diffOffsetY }, "beam-shot")
	   								.to($('#laser-beam'), .15, { opacity: 0 }, "beam-shot+=.2")
	   								.set($('#laser-beam'), { opacity: 0, x: 0, y: 0 })
	   								.add(asteroidBurst)
	   							;
	   						
	   						function asteroidBurst(){
		   						
		   								randTile.childNodes[0].classList.add('hide');
		   								randTile.childNodes[1].classList.add('pop');
		   								randTile.childNodes[1].classList.add('delayed-hide');	
		   								
		   					}

	   					}
		   				

		   				//remove element from array
		   				tileRepo.splice(randTile, 1);
		   				popped.push(randTile);


		   				shuffle(tileRepo);
		   			}

		   	//added to end timeline
		      i++;
		      console.log(tileRepo.length);
		      if (tileRepo.length > 0) {
		         swPopLoop();
		         gameLoop = true;

		      }
		      setTimeout(function(){
		      	if (gameLoop == true && tileRepo.length == 0) {
		      		
		      		$('#winner').addClass('sw-on');
					swTL2();


		      	}
		      }, 1000);
		      //added to end timeline

		   }, 1000)	//<--pause between each iteration
		}
		
		swPopLoop(); 			

		}, 1000);
	}
	
}

//https://davidwalsh.name/javascript-once
function once(fn, context) { 
	var result;

	return function() { 
		if(fn) {
			result = fn.apply(context || this, arguments);
			fn = null;
		}

		return result;
	};
}

//https://davidwalsh.name/javascript-once - Usage
var realTestTL2 = once(function() {
	testTL2();
	playApplause();
});

function init () {
  
  tiles = [];
  boxes = [];
  bball = totalPlayers()[0];
  numBoxes = totalPlayers()[0].length;

  gameselected = totalPlayers()[1];
  
  // $('#container').empty();$
  createBoxes(numBoxes);
  appendBoxes(boxes);  
	
	//shuffle the array
  var winning = 0;
  var newTiles = tiles.splice(0);
  
  tiles = shuffle(newTiles);
  
  winning = tiles[0].element.id.substr(10);

  $('#bp-holder-' + winning + '').addClass('winner');
  
}

//
// CREATE TILE
// ====================================================================
var tileRepo = [];

function createTile(num, prepend) {
    
  var add = prepend ? ["prependTo", "unshift"] : ["appendTo", "push"];  
  var comp = parseInt(winNum);	//return winner for comparison
  var tile = '';

  var huey = ['red', 'yellow', 'green', 'blue', 'purple'];
  var ballColor = huey[Math.floor(Math.random() * huey.length)];
  var widthSize = ['small', 'medium', 'large'];
  var balloonWidth = widthSize[Math.floor(Math.random() * widthSize.length)];

  
  if(gameselected == "Bingo Balls"){
	  if(num == comp){
	  	tile = $("<img id='bb-" + num + "' class='winner bb-color-" + ballColor + " bb-grid-ball-num' src='css/img/bb-" + num + ".png' alt=''>")[add[0]](container)[0];
	  	
	  } else {
	  	tile = $("<img id='bb-" + num + "' class='not-winner bb-color-" + ballColor + " bb-grid-ball-num' src='css/img/bb-" + num + ".png' alt=''>")[add[0]](container)[0];
	  	tileRepo.push(tile);
	  }
	}

	if(gameselected == "Balloon Pop"){
		if(num == comp){
			tile = $("<div id='bp-holder-" + num + "' class='winner bp-holder bp-" + balloonWidth + "'><img id='bp-" + num + "' class='bp bp-" + balloonWidth + " bp-grid-ball-num' src='css/img/bp-" + num + ".png' alt=''></div>")[add[0]](container)[0];
		} else {
			tile = $("<div id='bp-holder-" + num + "' class='not-winner bp-holder bp-" + balloonWidth +"'><img id='bp-" + num + "' class='not-winner bp bp-" + balloonWidth + " bp-grid-ball-num' src='css/img/bp-" + num + ".png' alt=''><img class='bp-pop' src='css/img/bp-pop.png'/></div>")[add[0]](container)[0];
			tileRepo.push(tile);
		}	
	}
	if(gameselected == "Space Wars"){
		if(num == comp){
			tile = $("<div id='bp-holder-" + num + "' class='winner bp-holder sw-holder bp-" + balloonWidth + "'><img id='sw-" + num + "' class='bp sw-" + balloonWidth + " sw-grid-ball-num' src='css/img/sw-" + num + ".png' alt=''></div>")[add[0]](container)[0];
		} else {
			tile = $("<div id='bp-holder-" + num + "' class='not-winner bp-holder sw-holder bp-" + balloonWidth +"'><img id='sw-" + num + "' class='not-winner bp sw-" + balloonWidth + " sw-grid-ball-num' src='css/img/sw-" + num + ".png' alt=''><img class='sw-pop' src='css/img/sw-laser.png'/></div>")[add[0]](container)[0];
			tileRepo.push(tile);
		}	
	}

  // tileRepo.push(tile);
  
  TweenLite.set(tile, { x: "+=0" });
     
  tiles[add[1]]({      
    transform: tile._gsTransform,
    element: tile,
    num: num,
    x: tile.offsetLeft,
    y: tile.offsetTop
  });
  
  return tile;
}

//
// REORDER TILES
// ====================================================================
function reorderTiles(shuffled) {
    
  var total = tiles.length;
  
  var i = total;
  
  while (i--) {
    
    var tile = tiles[i];
        
    tile.x = tile.element.offsetLeft;
    tile.y = tile.element.offsetTop;
        
    container.removeChild(tile.element);
  }
  
  shuffled ? shuffle(tiles) : tiles.sort(sortOrder);
    
  for (var i = 0; i < total; i++) {
    
    var tile = tiles[i];
        
    var lastX = tile.x;
    var lastY = tile.y;
    
    container.appendChild(tile.element);
    
    tile.x = tile.element.offsetLeft;
    tile.y = tile.element.offsetTop;
    
    var dx = tile.transform.x + lastX - tile.x;
    var dy = tile.transform.y + lastY - tile.y;   
    
    TweenLite.fromTo(tile.element, 1.25, { x: dx, y: dy }, { 
      x: 0, 
      y: 0, 
      ease: ease, 
      // delay: 1,
      immediateRender: true
    });    
  }  

}

function bpSequence(){
	// init();
	
	bpPop();
}

function playApplause(){
	var bpApplause = document.createElement('audio');
        bpApplause.setAttribute('src', 'audio/021514158-game-show-applause.mp3');
        //ause.load()

        $.get();

        bpApplause.addEventListener("load", function() {
            bpApplause.play();
        }, true);
        
        bpApplause.setAttribute('autoplay', 'autoplay');
	        //bpAppl
}

//BP POP
// ====================================================================

function bpPop(){
	// init();
	// $('.overlay-container').trigger('click');  	
	console.log('bpPop started');

	var i = 1;
	var dur = 500;

	setTimeout(function(){

		function bpPopLoop () {
		// setTimeout(function(){

	   setTimeout(function () {
	      
	   		var popped = []
	   		// for(var i = 0; i < tileRepo.length; i++){
	   			var randTile = tileRepo[0];

	   			if(tileRepo.length != 0){

	   				//don't remove the winning balloon
	   				if(randTile.childNodes[0].classList.contains('winner')){
	   					randTile = tileRepo[1];
	   					console.log('winner');
	   					console.log(tileRepo[0].childNodes[0]);
	   				}

	   				//switch classes to perform pop animation
	   				for(var j = 0; j < randTile.childNodes.length; j++){

	   					if(randTile.childNodes[j].classList.contains('bp-pop')){

	   						randTile.childNodes[0].classList.add('hide');
	   						randTile.childNodes[j].classList.add('pop');
	   						randTile.childNodes[j].classList.add('delayed-hide');

	   					}
	   				}

	   				//remove element from array
	   				tileRepo.splice(randTile, 1);
	   				popped.push(randTile);


	   				shuffle(tileRepo);
	   			}

	   	//added to end timeline
	      i++;
	      if(tileRepo.length <= 2){
	      	dur = 1500;
	      }
	      console.log(tileRepo.length);
	      if (tileRepo.length > 0) {
	         bpPopLoop();
	         gameLoop = true;

	      }
	      setTimeout(function(){
	      	if (gameLoop == true && tileRepo.length == 0) {
	      		
	      		$('#winner').addClass('bp-on');
				bpTL2();
				playApplause();

	      	}
	      }, 1000);
	      //added to end timeline

	   }, dur)
	}
	bpPopLoop(); 

	}, 1000);

}
//
// DROP TILES
// ====================================================================
var dropArr = []
var dropNum = 3;
function dropTiles(){

	// setTimeout(function(){


		var fallen = [];
		//credit to jobney
		// fallen = tileRepo.splice(0, dropNum);

		for(var k = 0; k < dropNum; k++){

			var randTile = tileRepo[k];
			
			tileRepo.splice(k, 1);

			fallen.push(randTile);

		}
		console.log('fallen');
		console.log(fallen);

		TweenLite.to(fallen, .75, {
			y: 1000,
			autoAlpha: 0,
			delay: 1,
			ease: Power2.easeIn
		});
	// },1000);
}

//
// SORT
// ====================================================================
function sortOrder(a, b) {
  return a.num - b.num;
}

function createBoxes (num) {

  for(i = 0; i < numBoxes; i++){
  	boxes.push(bball[i].id.substr(2));
  }

  boxes = boxes.splice(shuffle(boxes));
}

function addBoxes (num) {
  
  var numBoxes = boxes.length;
  var newBoxes = [];
  
  for (var i = 0; i < num; i += 1) {
    newBoxes.push(numBoxes + i + 1);
    boxes.push(numBoxes + i + 1);
  }
 
  return newBoxes;
}

function appendBoxes (collection, isShuffle) {  
    
  var tl = new TimelineLite();

  collection = shuffle(collection)
  
  collection.forEach(function(num, i) {
        
    var tile = createTile(num);

    tl.from(tile, duration, {
      opacity: 0, 
      scale: 0,
      ease: Sine.easeIn      
    }, '-=' + (duration - delay));        
  });

}

function shuffle (array) {
  
  var currentIndex = array.length;
  var temporaryValue;
  var randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}


